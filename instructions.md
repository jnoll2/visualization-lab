---
author: 'Dr. John Noll'
course: 7COM1079
date: '\today'
documentclass: hitec
fontfamily: opensans
fontsize: 12
institute: University of Hertfordshire
module: 7COM1079
subtitle: '7COM1079 -- Team Research and Development Project'
term: 'fall-19'
title: Visualization
---

Learning Objectives
-------------------

After this lab, you will be able to create basic data visualizations in
SPSS:

-   scatterplot;
-   boxplot;
-   bar chart;
-   histogram;
-   pie chart! (Maybe not.)

Import Data
-----------

### Import the Dataset

After increasing objection to the Cricket batting statistics, we are
switching to a new dataset this week: the London Borough Profiles for
2016 (london-borough-profiles-2016\_Data\_set.csv) originally from
Kaggle.

The data have been cleaned for you, so you just have to import them into
SPSS.

![Import as CSV](spss-import-as-csv.png)\

### Edit the dataset.

1.  The numbers have too many decimal places. Change to one or two:

    ![Decimal places](spss-adjust-decimals.png)\

2.  Change data types:

    As usual, some columns are not interpreted correctly:

    ![Change data type](spss-change-data-type.png)\

3.  Change data category ('measure'):

    Also change 'Measure' to 'Scale':

    ![Change measure](spss-change-measure.png)\

### Descriptive Statistics

As as sanity check, compute descriptive statistics on the income
variable:

![Compute descriptive stats](spss-compute-desc-stats.png)\

Q: Why are the median and mean close?

The central limit theorem says that means of samples of independent
random variables will tend to be normally distributed.

So even though the mean income tends to be skewed toward the high end
because of the influence of a few very wealthy people, we are plotting
the *means* of income in the various boroughs (not the actual incomes);
these will tend toward a normal distribution.

Visualizations
--------------

### Scatterplot: Happiness vs. Income

1.  For all plots, use "Legacy Dialogs":

    ![Graphs-\>Legacy
    Dialogs-\>Scatterplots](spss-legacy-dialog-scatterplot.png)\

    Note: scatterplots only really make sense when there is an
    independent variable and a dependent variable.

2.  Select the *independent* variable (income):

    ![Scatterplot independent variable](spss-scatterplot-indep-var.png)\

3.  Select the *dependent* variable (happiness):

    ![Scatterplot dependent variable](spss-scatterplot-dep-var.png)\

    You should see something like this:

    ![Scatterplot output](spss-scatterplot-output.png)\

4.  Add a "reference line:"

    ![Scatterplot linear model](spss-scatterplot-lm.png)\

    Linear model output:

    ![Scatterplot linear model](spss-scatterplot-lm-output.png)\

    Q: Why does the graph cross the Y-axis at about 7.21 rather than
    7.19 as specified by the model?

### Boxplot

1.  Select "Graphs -\> Legacy Dialogs -\> Boxplot"

    ![Graphs-\>Legacy
    Dialogs-\>Boxplot](spss-legacy-dialog-boxplot.png)\

2.  Choose the "Simple" type:

    ![Boxplot dialog](spss-legacy-dialog-boxplot-dialog.png)\

    You should see output like this:

    ![Boxplot output](spss-boxplot-output.png)\

### Bar chart

1.  Select "Graphs -\> Legacy Dialogs -\> Bar"

2.  Choose "Simple" format:

    ![Barchart dialog](spss-barchart-dialog.png)\

    You should see something like this:

    ![Barchart output](spss-barchart-output.png)\

    Q: what happened to all of London's millionaires?

### Histogram

1.  Select "Graphs -\> Legacy Dialogs -\> Historgram"

    ![Graphs-\>Legacy
    Dialogs-\>Histogram](spss-legacy-dialog-histogram.png)\

    You should see something like this:

    ![Histogram output](spss-histogram-output.png)\

2.  Add output with normal plot:

    ![Histogram output normal](spss-histogram-output-normal.png)\

### Pie Charts!

1.  Select individual cases for the "slices":

    ![Pie-chart dialog](spss-pie-chart-dialog.png)\

    You should see something like this:

    ![Pie-chart](spss-pie-chart-output.png)\

    Q: Which is better: Pie Chart or Bar Chart?

Summary
-------

-   Use scatterplots for visualizing (potential) correlation (interval
    data).
-   Use boxplots for visualizing variability (interval or ordinal data).
-   Use barplots for visualizing relative values (frequencies, amounts,
    mean, median) of categories (interval or ordinal vs. category).
-   Use histograms for visualizing frequencies or values against
    interval or ordinal 'bins.'
-   Use pie charts for visualizing gross differences (or emphasizing
    fine differences).
